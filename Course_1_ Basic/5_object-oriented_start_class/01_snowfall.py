# -*- coding: utf-8 -*-

import simple_draw as sd
import random

# Шаг 1: Реализовать падение снежинки через класс. Внести в методы:
#  - создание снежинки с нужными параметрами
#  - отработку изменений координат
#  - отрисовку

sd.resolution = (1200, 800)

# class Snowflake:
#     x = random.randint(0, 600)
#     y = 600
#
#     def clear_previous_picture(self):
#         sd.clear_screen()
#
#     def move(self, y_move, x_move):
#         self.y -= y_move
#         self.x -= x_move
#
#     def draw(self):
#         point = sd.get_point(self.x, self.y)
#         sd.snowflake(point, length=50)
#

    # TODO здесь ваш код




#
# while True:
#     flake.clear_previous_picture()
#     flake.move(5, -2)
#     flake.draw()
#     # if not flake.can_fall():
#     #     break
#     sd.sleep(0.1)
#     if sd.user_want_exit():
#         break

# шаг 2: создать снегопад - список объектов Снежинка в отдельном списке, обработку примерно так:


class Snowflake:
    x = []
    y = []

    for num in range(0, 1200, 60):
        x.append(num)

    for _ in range(20):
        num1 = random.randint(770, 800)
        y.append(num1)


    def clear_previous_picture(self):
        sd.clear_screen()

    def move(self, y_move, x_move, y1, x1):
        y1 -= y_move
        x1 -= x_move

    def draw(self):
        point = sd.get_point(self.x, self.y)
        sd.snowflake(point, length=50)

flakes = []



for _ in range(20):
    flake = Snowflake()
    flakes.append(flake)


coordinate = 0
list_coordinate = 0
for flake in flakes:
    y1 = flake.y[coordinate]
    x1 = flake.x[coordinate]
    flake.clear_previous_picture()
    flake.move(5, -2, y1, x1)
    flake.draw()
    sd.sleep(0.1)
    if sd.user_want_exit():
        break
    coordinate += 1
sd.pause()
