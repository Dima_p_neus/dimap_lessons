# -*- coding: utf-8 -*-

# Создать прототип игры Алхимия: при соединении двух элементов получается новый.
# Реализовать следующие элементы: Вода, Воздух, Огонь, Земля, Шторм, Пар, Грязь, Молния, Пыль, Лава.
# Каждый элемент организовать как отдельный класс.
# Таблица преобразований:
#   Вода + Воздух = Шторм
#   Вода + Огонь = Пар
#   Вода + Земля = Грязь
#   Воздух + Огонь = Молния
#   Воздух + Земля = Пыль
#   Огонь + Земля = Лава

# Сложение элементов реализовывать через __add__
# Если результат не определен - то возвращать None
# Вывод элемента на консоль реализовывать через __str__
#
# Примеры преобразований:
#   print(Water(), '+', Air(), '=', Water() + Air())
#   print(Fire(), '+', Air(), '=', Fire() + Air())

# TODO здесь ваш код

# Усложненное задание (делать по желанию)
# Добавить еще элемент в игру.
# Придумать что будет при сложении существующих элементов с новым.



#
#
#
# class alchemy:
#     """ Рюкзак """
#
#
#     def __init__(self, thing=None):
#         self.thing1 = thing
#
#     def __str__(self):
#         if alchemy.thing1 == 'Вода' and alchemy.thing2 == 'Воздух':
#             return 'Шторм: ' + ', '.join(self.content)
#         # return 'Пар: ' + ', '.join(self.content)
#         # return 'Грязь: ' + ', '.join(self.content)
#         # return 'Молния: ' + ', '.join(self.content)
#         # return 'Пыль: ' + ', '.join(self.content)
#         # return 'Лава: ' + ', '.join(self.content)
#
#
#     def __add__(self, other):
#         thing2 = alchemy()
#         thing2.extend(self.thing1)
#         thing2.thing1.extend(other.thing1)
#         return new_obj
#
#
# alchemy.thing1 = alchemy(thing='Вода')
# alchemy.thing2 = alchemy(thing='Воздух')
# result = alchemy.thing1 + alchemy.thing2
# print(result)


#Вода + Воздух = Шторм
#   Вода + Огонь = Пар
#   Вода + Земля = Грязь
#   Воздух + Огонь = Молния
#   Воздух + Земля = Пыль
#   Огонь + Земля = Лава


class alchemy:
    """ Рюкзак """

    def __init__(self, thing=None):
        self.content = []
        if thing:
            self.content.append(thing)

    def __str__(self):
        if self.content[0] == 'Вода' and self.content[1] == 'Воздух':
            return 'Шторм: ' + ', '.join(self.content)
        elif self.content[0] == 'Вода' and self.content[1] == 'Огонь':
            return 'Пар: ' + ', '.join(self.content)
        elif self.content[0] == 'Вода' and self.content[1] == 'Земля':
            return 'Грязь: ' + ', '.join(self.content)
        elif self.content[0] == 'Воздух' and self.content[1] == 'Огонь':
            return 'Молния: ' + ', '.join(self.content)
        elif self.content[0] == 'Воздух' and self.content[1] == 'Земля':
            return 'Пыль: ' + ', '.join(self.content)
        elif self.content[0] == 'Огонь' and self.content[1] == 'Земля':
            return 'Лава: ' + ', '.join(self.content)

    def __add__(self, other):
        new_obj = alchemy()
        new_obj.content.extend(self.content)
        new_obj.content.extend(other.content)
        return new_obj


my_backpack = alchemy(thing='Вода')
son_backpack = alchemy(thing='Воздух')

my_backpack1 = alchemy(thing='Вода')
son_backpack2 = alchemy(thing='Огонь')

my_backpack3 = alchemy(thing='Вода')
son_backpack4 = alchemy(thing='Земля')

my_backpack5 = alchemy(thing='Воздух')
son_backpack6 = alchemy(thing='Огонь')

my_backpack7 = alchemy(thing='Воздух')
son_backpack8 = alchemy(thing='Земля')

my_backpack9 = alchemy(thing='Огонь')
son_backpack10 = alchemy(thing='Земля')

result = my_backpack + son_backpack
print(result)

result1 =my_backpack1 + son_backpack2
print(result1)
result2 =my_backpack3 + son_backpack4
print(result2)
result3 =my_backpack5 + son_backpack6
print(result3)
result4 =my_backpack7 + son_backpack8
print(result4)
result5 =my_backpack9 + son_backpack10
print(result5)

if result1 == result2:
    print("ok")