# class Bread_for_burger:
#
#     def __str__(self):
#         return 'Я хлеб'
#
#     def __add__(self, other, other_1, other_2, other_3):
#         return everything(part1=self, part2=other, part4=other_3)
#
# class Something_for_burger:
#     def __str__(self):
#         return 'I am something good and veeerrrryyy tasty'
#     def __add__(self, other):
#         return everything(part1=self, part2=other)
#
#
# class everything():
#     def __init__(self, part1, part2, part3, part4):
#         self.part1 = part1, part2, part3, part4
#         self.part2 = part2
#
#     def __str__(self):
#         return 'I am a big plate of food, everything on me is: ' + str(self.part1) + ' и ' + str(self.part2)
#
#
# burger = Bread_for_burger
# everything_else_on_the_plate = Something_for_burger
# everything_together = burger + everything_else_on_the_plate
# print(everything_together)
#
# class Bread_for_burger:
#
#     def __str__(self):
#         return 'Я хлеб'
#
#     def __add__(self, other):
#         return everything(part1=self, part2=other)
#
#
# class Something_for_burger:
#
#     def __str__(self):
#         return 'something tasty'
#
#     def __add__(self, other):
#         return everything(part1=self, part2=other)
#
#
# class everything:
#
#     def __init__(self, part1, part2):
#         self.part1 = part1
#         self.part2 = part2
#
#     def __str__(self):
#         return 'Im a burger. Im made from: ' + str(self.part1) + ' and ' + str(self.part2)
#
#
# burger = Bread_for_burger()
# everything_else_on_the_plate = Something_for_burger()
# result = burger + everything_else_on_the_plate
# print(result)


from random import randint, choice

#
# # посчитаем леммингов
# class Lemming:
#     pass
#
#
# total_lemmings = 0
# lemming_1 = Lemming()
# total_lemmings += 1
# lemming_2 = Lemming()
# total_lemmings += 1
# lemming_3 = Lemming()
# total_lemmings += 1
#
# family = []
# family_size = randint(16, 32)
# while len(family) < family_size:
#     new_lemming = Lemming()
#     family.append(new_lemming)
#     total_lemmings += 1
# print(total_lemmings)
#
#
# # пусть сам класс следит за количеством своих объектов
# class Lemming:
#     # можно определять атрибуты на уровне класса, тогда они "привязаны" к классу
#     total = 0
#
#     def __init__(self):
#         # обращаться - через именование класса
#         Lemming.total += 1
#
#
# family = []
# family_size = randint(16, 32)
# while len(family) < family_size:
#     new_lemming = Lemming()
#     family.append(new_lemming)
# print(Lemming.total)
#
#
# from random import randint, choice
#
#
# # Атрибут объекта перекрывает атрибут класса
# class Lemming:
#     names = ['Peter', 'Anna', 'Nik', 'Sofi', 'Denn', 'Lora', 'Bred']
#     tail_length = 20
#
#     def __init__(self):
#         self.tail_length = randint(15, 25)
#         self.name = choice(Lemming.names)
#
#     def __str__(self):
#         return 'Lemming ' + self.name + ' with tail ' + str(self.tail_length)
#
#
# print(Lemming.tail_length)
#
# new_lemming = Lemming()
# print(new_lemming.tail_length)
# print(new_lemming)
# new_lemming1 = Lemming()
# print(new_lemming1.tail_length)
# print(new_lemming1)
#

class SomeClass:
    x = 10

    def __init__(self):
        x = 14

    def method_one(self):
        #x = 23
        print('method_one', x)

    def method_two(self):
        x = 34
        def func_one():
            #x = 56
            print('func_one', x)
        func_one()
        print('method_two', x)


x = 12
obj = SomeClass()
obj.method_one()
obj.method_two()
print('global', x)