# -*- coding: utf-8 -*-

# Доработать практическую часть урока 5_object-oriented_start_class/python_snippets/08_practice.py

# Необходимо создать класс кота. У кота есть аттрибуты - сытость и дом (в котором он живет).
# Кот живет с человеком в доме.
# Для кота дом характеризируется - миской для еды и грязью.
# Изначально в доме нет еды для кота и нет грязи.

# Доработать класс человека, добавив методы
#   подобрать кота - у кота появляется дом.
#   купить коту еды - кошачья еда в доме увеличивается на 50, деньги уменьшаются на 50.
#   убраться в доме - степень грязи в доме уменьшается на 100, сытость у человека уменьшается на 20.
# Увеличить кол-во зарабатываемых человеком денег до 150 (он выучил пайтон и устроился на хорошую работу :)

# Кот может есть, спать и драть обои - необходимо реализовать соответствующие методы.
# Когда кот спит - сытость уменьшается на 10
# Когда кот ест - сытость увеличивается на 20, кошачья еда в доме уменьшается на 10.
# Когда кот дерет обои - сытость уменьшается на 10, степень грязи в доме увеличивается на 5
# Если степень сытости < 0, кот умирает.
# Так же надо реализовать метод "действуй" для кота, в котором он принимает решение
# что будет делать сегодня

# Человеку и коту надо вместе прожить 365 дней.

# TODO здесь ваш код

# Усложненное задание (делать по желанию)
# Создать несколько (2-3) котов и подселить их в дом к человеку.
# Им всем вместе так же надо прожить 365 дней.

# (Можно определить критическое количество котов, которое может прокормить человек...)
import random

from termcolor import cprint
class man:
    def __init__(self, name):
        self.name = name
        self.fullness = 50
        self.house = None
        self.happines = 10
        self.money = 0

    dead = 0

    def __str__(self):
        return 'Я - {}, сытость {}'.format(self.name, self.fullness,)


    def eat(self):
        if self.house.food >= 10:
            cprint('{} поел'.format(self.name), color='yellow')
            self.fullness += 25
            self.house.food -= 10
            self.happines += 10
        else:
            cprint('{} нет еды'.format(self.name), color='red')


    def work(self):
        cprint('{} сходил на работу'.format(self.name), color='blue')
        self.house.money += 150
        self.fullness -= 10
        self.happines =- 5

    def shopping(self):
        if self.house.money >= 75:
            cprint('{} сходил в магазин за едой'.format(self.name), color='magenta')
            self.house.money -= 50
            self.house.food += 50
        else:
            cprint('{} деньги кончились!'.format(self.name), color='red')


    def cat_food_shopping(self):
        if self.house.money >= 50:
            cprint('{} сходил в магазин за koshacej едой'.format(self.name), color='magenta')
            self.house.money -= 10
            Home.cat_food += 50
        else:
            cprint('{} деньги кончились!'.format(self.name), color='red')


    def programm(self):
        cprint('{} сходил на programiravanie'.format(self.name), color='blue')
        self.house.money += 35
        self.fullness -= 5
        self.happines += 10


    def sport(self):
        cprint('{} poigral tennis'.format(self.name), color='blue')
        self.fullness -= 15
        self.happines += 10


    def read_a_book(self):
        cprint('{} cital knigu'.format(self.name), color='blue')
        self.fullness -= 2
        self.happines += 8


    def go_out(self):
        cprint('{} posol vstretitsa s druziami'.format(self.name), color='blue')

    def relax(self):
        cprint('{} смотрел TV целый день'.format(self.name), color='green')
        self.fullness -= 10

    def go_to_the_house(self, house):
        self.house = house
        self.fullness -= 10
        cprint('{} Вьехал в дом'.format(self.name), color='cyan')

    def clean_house(self):
        cprint('{} ubral doma'.format(self.name), color='green')
        self.fullness -= 20
        Home.dirt = 0

    def act(self):
        dice = random.randint(1, 10)
        if self.fullness <= 0:
            cprint('{} умер...'.format(self.name), color='red')
            man.dead = 1
            return
        if self.fullness < 20:
            self.eat()
        elif Home.dirt >= 100:
            self.clean_house()
        elif self.house.food < 10 or self.house.food == 0:
            self.shopping()
        elif self.house.cat_food < 10:
            self.cat_food_shopping()
        elif self.house.money < 50:
            self.work()
        elif self.money < 50:
            self.work()
        elif dice == 1:
            self.shopping()
        elif dice == 2:
            self.eat()
        elif dice == 3:
            self.programm()
        elif dice == 4:
            self.sport()
        elif dice == 5:
            self.read_a_book()
        elif dice == 6:
            self.go_out()
        else:
             self.shopping()


class cat:

    def __init__(self, name):
        self.name = name
        self.fullness = 50
        self.house = None
        self.happines = 10
        self.satisfaction = 10



    def __str__(self):
        return 'Я - {}, сытость {}'.format(self.name, self.fullness,)

    def vyprasovat_edu(self):
        cprint('{} vyprosil edy'.format(self.name), color='white')
        self.fullness += 15
        self.happines += 10
        self.satisfaction += 10
        Home.cat_food -= 15

    def sleap(self):
        cprint('{} pospal'.format(self.name), color='white')
        self.happines += 10
        self.satisfaction += 10

    def play(self):
        cprint('{} poigral'.format(self.name), color='white')
        self.fullness -= 10
        self.happines += 10
        self.satisfaction += 10
        Home.dirt += 5


    def gulat(self):
        cprint('{} pogulal'.format(self.name), color='white')
        self.fullness -= 10
        self.happines += 10
        self.satisfaction += 8


    def cat_act(self):
        if self.fullness < 10:
            self.vyprasovat_edu()
        if self.happines or self.satisfaction < 10:
            dice = random.randint(1, 3)
            if dice == 1:
                self.sleap()
            elif dice == 2:
                self.play()
            elif dice == 3:
                self.gulat()
        else:
            self.sleap()




class Home:

    def __init__(self):
        self.food = 50
        self.money = 0

    dirt = -10

    cat_food = 50

    def __str__(self):
        return 'В доме еды осталось {}, ' \
               'денег осталось {}, edy dla kota ostalos {}, grazi:{}'.format(self.food,
                   self.money, Home.cat_food, self.dirt)


citizens = [
    man(name='Бивис'),
    man(name='Кенни'),
]

cats = [
    cat(name='cat'),
    cat(name='cat1'),
]
cat_food = 50
my_sweet_home = Home()
for citisen in citizens:
    citisen.go_to_the_house(house=my_sweet_home)


for day in range(1, 366):
    if man.dead >= 1:
        break
    print('================ день {} =================='.format(day))
    for citisen in citizens:
        citisen.act()
        citisen.act()
    for cat in cats:
        cat.cat_act()
    print('--- в конце дня ---')
    for citisen in citizens:
        print(citisen)
    print(my_sweet_home)


