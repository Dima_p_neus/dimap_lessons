# -*- coding: utf-8 -*-
import random

import simple_draw as sd

sd.resolution = (1200, 800)


# 1) Написать функцию draw_branches, которая должна рисовать две ветви дерева из начальной точки
# Функция должна принимать параметры:
# - точка начала рисования,
# - угол рисования,
# - длина ветвей,
# Отклонение ветвей от угла рисования принять 30 градусов,


# 2) Сделать draw_branches рекурсивной
# - добавить проверку на длину ветвей, если длина меньше 10 - не рисовать
# - вызывать саму себя 2 раза из точек-концов нарисованных ветвей,
#   с параметром "угол рисования" равным углу только что нарисованной ветви,
#   и параметром "длинна ветвей" в 0.75 меньшей чем длина только что нарисованной ветви

# 3) Запустить вашу рекурсивную функцию, используя следующие параметры:
# root_point = sd.get_point(300, 30)
# draw_branches(start_point=root_point, angle=90, length=100)

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# Возможный результат решения см results/exercise_04_fractal_01.jpg

# можно поиграть -шрифтами- цветами и углами отклонения

# 4) Усложненное задание (делать поосле зачета первой части)
# - сделать рандомное отклонение угла ветвей в пределах 40% от 30-ти градусов
# - сделать рандомное отклонение длины ветвей в пределах 20% от коэффициента 0.75
# Возможный результат решения см results/exercise_04_fractal_02.jpg
point_0 = sd.get_point(600, 5)

def branch(point, angle, length, delta):
    if length < 10:
        return
    if length < 40:
        c = sd.COLOR_GREEN
    else:
        c = sd.COLOR_DARK_ORANGE
    v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
    v1.draw(c)
    next_point = v1.end_point
    next_angle = angle - (delta + random.randint(-15,15))
    next_angle_2 = angle + (delta + random.randint(-15,15))
    next_length = length * (random.randint(6, 9) / 10)
    branch(point=next_point, angle=next_angle, length=next_length, delta=delta)
    branch(point=next_point, angle=next_angle_2, length=next_length, delta=delta)

# Пригодятся функции
# sd.random_number()
branch(point_0, angle=90, length=200, delta=sd.random_number(20, 50))

sd.pause()