# -*- coding: utf-8 -*-

print("0=COLOR_RED, 1=COLOR_CYAN, 2=COLOR_WHITE, 3=COLOR_BLUE, 4=COLOR_ORANGE, 5=COLOR_YELLOW, 6=COLOR_PURPLE, 7=COLOR_GREEN")
i = int(input("Input number of color 0-7: "))

import simple_draw as sd
c = [sd.COLOR_RED, sd.COLOR_CYAN, sd.COLOR_WHITE, sd.COLOR_BLUE, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_PURPLE, sd.COLOR_GREEN]
sd.background_color = sd.COLOR_BLACK
sd.resolution = (800, 600)


# Добавить цвет в функции рисования геом. фигур. из упр 01_shapes.py
# (код функций скопировать сюда и изменить)
# Запросить у пользователя цвет фигуры посредством выбора из существующих:
#   вывести список всех цветов с номерами и ждать ввода номера желаемого цвета.
# Потом нарисовать все фигуры этим цветом

# Пригодятся функции
# sd.get_point()
# sd.line()
# sd.get_vector()
# и константы COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_CYAN, COLOR_BLUE, COLOR_PURPLE
# Результат решения см results/exercise_02_global_color.jpg
#TODO Задание выполнить
# TODO здесь ваш код



def square(point, angle=0, len = 100):
    v1 = sd.get_vector(start_point=point, angle=angle, length=len, width=3)
    v1.draw(c[i])
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 90, length=len, width=3)
    v2.draw(c[i])
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 180, length=len, width=3)
    v3.draw(c[i])
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle + 270, length=len, width=3)
    v4.draw(c[i])

def petiugolnik(point, angle=0, len = 100):
    v1 = sd.get_vector(start_point=point, angle=angle, length=len, width=3)
    v1.draw(c[i])
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 72, length=len, width=3)
    v2.draw(c[i])
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 144, length=len, width=3)
    v3.draw(c[i])
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle + 216, length=len, width=3)
    v4.draw(c[i])
    v5 = sd.get_vector(start_point=v4.end_point, angle=angle + 288, length=len, width=3)
    v5.draw(c[i])

def sestiugolnik(point, angle, len):
    v1 = sd.get_vector(start_point=point, angle=angle, length=len, width=3)
    v1.draw(c[i])
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 60, length=len, width=3)
    v2.draw(c[i])
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 120, length=len, width=3)
    v3.draw(c[i])
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle + 180, length=len, width=3)
    v4.draw(c[i])
    v5 = sd.get_vector(start_point=v4.end_point, angle=angle + 240, length=len, width=3)
    v5.draw(c[i])
    v6 = sd.get_vector(start_point=v5.end_point, angle=angle + 300, length=len, width=3)
    v6.draw(c[i])

def vseugolnik():
    point = point_0 = sd.get_point(400, 300)
    square(point, angle=0, len=100)

    point = point_0 = sd.get_point(100, 200)
    petiugolnik(point, angle=0, len=100)

    point = point_0 = sd.get_point(250, 100)
    sestiugolnik(point, angle=0, len=100)

vseugolnik()

sd.pause()