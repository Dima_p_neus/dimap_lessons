# -*- coding: utf-8 -*-
import time

import simple_draw as sd
import random
sd.resolution = (1200, 800)
# На основе кода из практической части реализовать снегопад:
# - создать списки данных для отрисовки N снежинок
# - нарисовать падение этих N снежинок
# - создать список рандомных длинн лучей снежинок (от 10 до 100) и пусть все снежинки будут разные

# N = 20.


# Пригодятся функции
# sd.get_point()
# sd.snowflake()
# sd.sleep()


# Примерный алгоритм отрисовки снежинок
#   навсегда
#     очистка экрана
#     для индекс, координата_х из списка координат снежинок
#       получить координата_у по индексу
#       создать точку отрисовки снежинки
#       нарисовать снежинку цветом фона
#       изменить координата_у и запомнить её в списке по индексу
#       создать новую точку отрисовки снежинки
#       нарисовать снежинку на новом месте белым цветом
#     немного поспать
#     если пользователь хочет выйти
#       прервать цикл

# TODO здесь ваш код
# x =[]
# y=[]
#
# for num in range(0, 1200, 60):
#     x.append(num)
# print(x)
#
# for num1 in range(20):
#     num1 = random.randint(770, 800)
#     y.append(num1)
# print(y)
#
#
# c = 0
# while True:
#     for _ in range(80):
#         sd.clear_screen()
#         for num_x in range(len(x)):
#             point = sd.get_point(x[num_x], y[num_x])
#             sd.snowflake(center=point, length=20)
#             if y[num_x] > 10:
#                 y[num_x] -= random.randint(5, 20)
#                 for _ in range(2):
#                     x[num_x] += random.randint(5, 15)
#                 for _ in range(2):
#                     x[num_x] -= random.randint(5, 15)
#             else:
#                 y[num_x] += 790
#
#
#         time.sleep(0.002)
#
#
#
#
#
#
# print(x)
#
#
#
#
# sd.pause()
# y = 500
# x = 100
#
# y2 = 450
# x2 = 150
#
# y3 = 500
# x3 = 150
#
# y4 = 450
# x4 = 100
# while True:
#     for _ in range(10):
#
#         sd.clear_screen()
#         point = sd.get_point(x, y)
#         sd.snowflake(center=point, length=50)
#         y -= 15
#         if y < 50:
#             break
#         x = x + 5
#
#         point2 = sd.get_point(x2, y2)
#         sd.snowflake(center=point2, length=30)
#         y2 -= 10
#         if y2 < 50:
#             break
#         x2 = x2 + 20
#         x = x + 15
#         sd.sleep(0.2)
#         if sd.user_want_exit():
#              break
#         point = sd.get_point(x + 100, y)
#     # реализовать падение одной
# sd.sleep(0.5)
# sd.pause()

# Часть 2 (делается после зачета первой части)
#
# Ускорить отрисовку снегопада
# - убрать clear_screen() из цикла
# - в начале рисования всех снежинок вызвать sd.start_drawing()
# - на старом месте снежинки отрисовать её же, но цветом sd.background_color
# - сдвинуть снежинку
# - отрисовать её цветом sd.COLOR_WHITE на новом месте
# - после отрисовки всех снежинок, перед sleep(), вызвать sd.finish_drawing()

# Усложненное задание (делать по желанию)
# - сделать рандомные отклонения вправо/влево при каждом шаге
# - сделать сугоб внизу экрана - если снежинка долетает до низа, оставлять её там,
#   и добавлять новую снежинку

# TODO здесь ваш код
# sd.background_color=sd.COLOR_BLACK
# x = []
# y = []
#
# for num in range(0, 1200, 60):
#     x.append(num)
# print(x)
#
# for num1 in range(20):
#     num1 = random.randint(770, 800)
#     y.append(num1)
# print(y)
# c = 0
#
# while True:
#     num_x = 0
#     for _ in range(80):
#          for num_x in range(len(x)):
#              if y[num_x] > 10:
#                   c = random.randint(5, 20)
#                   y[num_x] -= c
#              point = sd.get_point(x[num_x], y[num_x])
#              sd.snowflake(center=point, length=20, color=sd.COLOR_WHITE)
#             # if y[num_x] > 10:
#             #     c = random.randint(5, 20)
#             #     y[num_x] -= c
#              for _ in range(2):
#                 a = random.randint(5, 15)
#                 x[num_x] += a
#              for _ in range(2):
#                 b = random.randint(5, 15)
#                 y[num_x] -= b
#              print(y[num_x])
#              y[num_x] += c
#              y[num_x] += b
#              print(y[num_x])
#              point = sd.get_point(x[num_x], y[num_x])
#              sd.snowflake(center=point, length=20, color=sd.COLOR_BLACK)
#              if y[num_x] < 10:
#                  break
#
#
#
#          time.sleep(0.002)
#
#
#
#
#
#
# print(x)
#
#
#
sd.background_color=sd.COLOR_BLACK
x =[]
y=[]

for num in range(0, 1200, 60):
    x.append(num)
print(len(x))

for num1 in range(20):
    num1 = random.randint(770, 800)
    y.append(num1)
print(len(y))


leng = []
for num2 in range(20):
    num2 = random.randint(10, 60)
    leng.append(num2)
print(leng)

c = 0
print(len(leng))
while True:
    sd.start_drawing()
    f = random.randint(0, 100)
    for num_x in range(len(x)):
        point = sd.get_point(x[num_x], y[num_x])
        if y[num_x] > f:
            sd.snowflake(center=point, length=leng[num_x], color=sd.COLOR_BLACK)
    for num_x in range(len(x)):
        if y[num_x] > f:
            y[num_x] -= random.randint(5, 20)
            x[num_x] += random.randint(5, 20)
            x[num_x] -= random.randint(5, 20)
        else:
            y[num_x] += 790
        point = sd.get_point(x[num_x], y[num_x])

        sd.snowflake(center=point, length=leng[num_x])
    sd.finish_drawing()
    time.sleep(0.08)
    if sd.user_want_exit(0):
         break


