# -*- coding: utf-8 -*-
import simple_draw as sd
#TODO Задание выполнить
sd.resolution = (800, 800)


# Создать функцию которая нарисует подобие шахматной доски по двум параметрам.

# Функцию будет следующего вида   def desk(y, x):
# Где у = высота доски(количество квадратов по вертикали), х = ширина доски(количесво квадратов по горизонтали)
strx = 0
stry = 0
stepx = 100
stepy = 100
a = sd.COLOR_BLACK
for _ in range(8):
    for i in range(8 ):
        point1 = sd.get_point(strx, stry)
        point2 = sd.get_point(strx + stepx, stry + stepy)
        sd.rectangle(point1, point2, color=a, width=0)
        strx += stepx
        if a == sd.COLOR_BLACK:
            a = sd.COLOR_WHITE
        else:
            a = sd.COLOR_BLACK
    strx = 0
    stry += stepy
    if a == sd.COLOR_BLACK:
        a = sd.COLOR_WHITE
    else:
        a = sd.COLOR_BLACK

sd.pause()
