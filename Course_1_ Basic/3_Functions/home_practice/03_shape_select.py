


# Запросить у пользователя желаемую фигуру посредством выбора из существующих
#   вывести список всех фигур с номерами и ждать ввода номера желаемой фигуры.
# и нарисовать эту фигуру в центре экрана
#
# Код функций из упр 02_global_color.py скопировать сюда
# Результат решения см results/exercise_03_shape_select.jpg

# TODO Задание выполнить
# TODO здесь ваш код
print(
    "0=COLOR_RED, 1=COLOR_CYAN, 2=COLOR_WHITE, 3=COLOR_BLUE, 4=COLOR_ORANGE, 5=COLOR_YELLOW, 6=COLOR_PURPLE, 7=COLOR_GREEN")
i = int(input("Input number of color 0-7: "))

print("0=square, 1=petiugolnik, 2=sestiugolnik")
sh = int(input("Input number of shape 0-2: "))

import simple_draw as sd
sd.resolution = (800, 600)

c = [sd.COLOR_RED, sd.COLOR_CYAN, sd.COLOR_WHITE, sd.COLOR_BLUE, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_PURPLE,
     sd.COLOR_GREEN]
sd.background_color = sd.COLOR_BLACK
sd.resolution = (800, 600)


def square(point, angle=0, len=100):
    v1 = sd.get_vector(start_point=point, angle=angle, length=len, width=3)
    v1.draw(c[i])
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 90, length=len, width=3)
    v2.draw(c[i])
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 180, length=len, width=3)
    v3.draw(c[i])
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle + 270, length=len, width=3)
    v4.draw(c[i])


def petiugolnik(point, angle=0, len=100):
    v1 = sd.get_vector(start_point=point, angle=angle, length=len, width=3)
    v1.draw(c[i])
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 72, length=len, width=3)
    v2.draw(c[i])
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 144, length=len, width=3)
    v3.draw(c[i])
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle + 216, length=len, width=3)
    v4.draw(c[i])
    v5 = sd.get_vector(start_point=v4.end_point, angle=angle + 288, length=len, width=3)
    v5.draw(c[i])


def sestiugolnik(point, angle, len):
    v1 = sd.get_vector(start_point=point, angle=angle, length=len, width=3)
    v1.draw(c[i])
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 60, length=len, width=3)
    v2.draw(c[i])
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 120, length=len, width=3)
    v3.draw(c[i])
    v4 = sd.get_vector(start_point=v3.end_point, angle=angle + 180, length=len, width=3)
    v4.draw(c[i])
    v5 = sd.get_vector(start_point=v4.end_point, angle=angle + 240, length=len, width=3)
    v5.draw(c[i])
    v6 = sd.get_vector(start_point=v5.end_point, angle=angle + 300, length=len, width=3)
    v6.draw(c[i])


point = sd.get_point(400, 300)
if sh == 0:
    square(point, angle=0, len=100)
if sh == 1:
    petiugolnik(point, angle=0, len=100)
if sh == 2:
    sestiugolnik(point, angle=0, len=100)

sd.pause()
