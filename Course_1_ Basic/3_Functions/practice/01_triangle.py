# -*- coding: utf-8 -*-

# pip install simple_draw
import simple_draw as sd
sd.resolution = (800, 600)

# нарисовать треугольник из точки (300, 300) с длиной стороны 200

# point = sd.get_point(300, 300)
# length = 200
# v1 = sd.get_vector(start_point=point, angle=0, length=200, width=3)
# v1.draw()
# #
# v2 = sd.get_vector(start_point=v1.end_point, angle=120, length=200, width=3)
# v2.draw()
# # # # #
# v3 = sd.get_vector(start_point=v2.end_point, angle=240, length=200, width=3)
# v3.draw()
# sd.pause()

def triangle(point, angle=0, len = 100):
    v1 = sd.get_vector(start_point=point, angle=angle, length=len, width=3)
    v1.draw()
    v2 = sd.get_vector(start_point=v1.end_point, angle=angle + 120, length=len, width=3)
    v2.draw()
    v3 = sd.get_vector(start_point=v2.end_point, angle=angle + 240, length=len, width=3)
    v3.draw()
#
#
#
point_0 = sd.get_point(400, 300)
# triangle(point=point_0, angle=100, len=300)

for angle in range(0, 361, 10):
    triangle(point=point_0, angle=angle, len=200)
    sd.sleep(0.3)
#
#
sd.pause()

