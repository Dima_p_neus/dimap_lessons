# (определение функций)

# Написать функцию отрисовки смайлика по заданным координатам
# Форма рожицы-смайлика на ваше усмотрение (что-нибудь сложнее чем чем просто глаза и рот, минимально 15  объектов)

# Параметры функции: кордината X, координата Y, цвет.
# Вывести 10 смайликов в произвольных точках экрана.

# TODO твой код ниже
import random

import simple_draw as sd


sd.resolution = (1000, 1000)

def smile(x = 300, y = 300):
    point = sd.get_point(x, y)
    sd.circle(center_position=point, radius=50, width=0)
    r = 13
    point1 = sd.get_point(x-16, y-20)
    v1 = sd.get_vector(start_point=point1, angle=0, length=30, width=3)
    v1.draw(sd.COLOR_BLACK,3)
    v2 = sd.get_vector(start_point=v1.end_point, angle=30, length=15, width=3)
    v2.draw(sd.COLOR_BLACK, 3)
    v3 = sd.get_vector(start_point=v1.start_point, angle=150, length=15, width=3)
    v3.draw(sd.COLOR_BLACK, 3)
    for _ in range(2):
        point6 = sd.get_point(x + 20, y + 20)
        point7 = sd.get_point(x - 20, y + 20)
        sd.circle(center_position=point6, radius=r, color=sd.COLOR_WHITE, width=0)
        sd.circle(center_position=point7, radius=r, color=sd.COLOR_WHITE, width=0)
    for _ in range(2):
        point6 = sd.get_point(x + 20, y + 20)
        point7 = sd.get_point(x - 20, y + 20)
        sd.circle(center_position=point6, radius=5, color=sd.COLOR_BLACK, width=0)
        sd.circle(center_position=point7, radius=5, color=sd.COLOR_BLACK, width=0)
    for _ in range(2):
        point6 = sd.get_point(x + 20, y + 20)
        point7 = sd.get_point(x - 20, y + 20)
        sd.circle(center_position=point6, radius=18, color=sd.COLOR_BLACK, width=2)
        sd.circle(center_position=point7, radius=18, color=sd.COLOR_BLACK, width=2)
    point2 = sd.get_point(x-50, y+40)
    point3 = sd.get_point(x+50, y+55)
    sd.rectangle(point2, point3,color=sd.COLOR_BLACK, width=0)
    point4 = sd.get_point(x - 30, y + 55)
    point5 = sd.get_point(x + 30, y + 100)
    sd.rectangle(point4, point5, color=sd.COLOR_BLACK, width=0)
    point1 = sd.get_point(x - 46, y + 28)
    v = sd.get_vector(start_point=point1, angle=330, length=12, width=1)
    v.draw(sd.COLOR_BLACK, 3)
    point1 = sd.get_point(x + 42, y + 28)
    v4 = sd.get_vector(start_point=point1, angle=208, length=8, width=1)
    v4.draw(sd.COLOR_BLACK, 3)
    point1 = sd.get_point(x - 2, y + 22)
    v5 = sd.get_vector(start_point=point1, angle=0, length=5, width=1)
    v5.draw(sd.COLOR_BLACK, 3)
    point1 = sd.get_point(x - 1, y - 1)
    v6 = sd.get_vector(start_point=point1, angle=60, length=10, width=1)
    v6.draw(sd.COLOR_BLACK)
    v7 = sd.get_vector(start_point=v6.end_point, angle=60 + 120, length=10, width=1)
    v7.draw(sd.COLOR_BLACK)
    v8 = sd.get_vector(start_point=v7.end_point, angle=60 + 240, length=10, width=1)
    v8.draw(sd.COLOR_BLACK)
print(type(sd.random_point()))
for _ in range(10):
    x = random.randint(100, 900)
    y = random.randint(100, 900)
    smile(x, y)

sd.pause()

#TODO Задание доделать