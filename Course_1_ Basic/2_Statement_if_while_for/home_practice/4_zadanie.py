# (цикл while)

# Ежемесячная стипендия студента составляет educational_grant крон, а расходы на проживание превышают стипендию
# и составляют expenses крон в месяц. Рост цен ежемесячно увеличивает расходы на 3%, кроме первого месяца
# Составьте программу расчета суммы денег, которую необходимо единовременно попросить у родителей,
# чтобы можно было прожить учебный год (10 месяцев), используя только эти деньги и стипендию.
# Формат вывода:
#   Студенту надо попросить ХХХ.ХХ крон

