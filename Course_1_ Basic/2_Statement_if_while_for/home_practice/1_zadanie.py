# (if/elif/else)

# По номеру месяца вывести кол-во дней в нем (без указания названия месяца, в феврале 28 дней)
# Результат проверки вывести на консоль
# Если номер месяца некорректен - сообщить об этом

# Номер месяца получать от пользователя следующим образом
# user_input = input("Введите, пожалуйста, номер месяца: ")
# month = int(user_input)
# print('Вы ввели', month)
months = ['febuary', 'january', 'marc', 'april', 'may', 'june', 'july', 'august', 'september', 'oktober', 'december']
num_mon = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
monnum = int(input("Введите, пожалуйста, номер месяца: "))
monnum -= 1   #  monnum = monnum - 1
#print(monnum)
print('You have written month ' + months[monnum] + ' and it have ' + str(days[monnum]) + ' days.')