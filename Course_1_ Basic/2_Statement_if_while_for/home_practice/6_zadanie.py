

# Нарисовать стену из кирпичей. Размер кирпича - 100х50
# Использовать вложенные циклы for


# Подсказки:
#  Для отрисовки кирпича использовать функцию rectangle
#  Алгоритм должен получиться приблизительно такой:
#
#   цикл по координате Y
#       вычисляем сдвиг ряда кирпичей
#       цикл координате X
#           вычисляем правый нижний и левый верхний углы кирпича
#           рисуем кирпич

import simple_draw as sd

sd.resolution = 800, 800
strx = 0
stry = 0
stepx = 100
stepy = 50
for k in range(11):
    if k % 2 == 0:
        strx += stepx/2
    for i in range(6):
        point1 = sd.get_point(strx, stry)
        point2 = sd.get_point(strx + stepx, stry + stepy)
        sd.rectangle(point1, point2, color=sd.COLOR_RED, width=2)
        strx += stepx
    strx = 0
    stry += stepy
sd.pause()