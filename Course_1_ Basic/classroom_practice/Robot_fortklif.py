from spike import PrimeHub, LightMatrix, Button, StatusLight, ForceSensor, MotionSensor, Speaker, ColorSensor, App, DistanceSensor, Motor, MotorPair
from spike.control import wait_for_seconds, wait_until, Timer
from math import *

hub = PrimeHub()

hub.light_matrix.show_image('HAPPY')

motor_pair = MotorPair('C', 'D')

motor_fortklif = Motor('A')
button=ForceSensor('F')
count = 0

step = 50
while count < 3:
    if count >= 1:
        motor_pair.move_tank(9, 'cm', left_speed=25, right_speed=-25)
        motor_pair.move(30, 'cm', steering=0, speed=25)
        motor_pair.move_tank(9, 'cm', left_speed=-25, right_speed=25)
    motor_pair.move(step, 'cm', steering=0, speed=25)
    motor_fortklif.run_for_degrees(-450, 30)
    motor_pair.move(step, 'cm', steering=0, speed=-25)
    motor_pair.move_tank(18, 'cm', left_speed=25, right_speed=-25)
    motor_pair.move(50, 'cm', speed=25)
    motor_fortklif.start(30)
    button.wait_until_pressed()
    motor_fortklif.stop()
    motor_pair.move(50, 'cm', steering=0, speed=-25)
    motor_pair.move_tank(18, 'cm', left_speed=25, right_speed=-25)
    count += 1
    step += 30