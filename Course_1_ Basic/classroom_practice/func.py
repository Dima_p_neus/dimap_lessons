# def some_func():
#     print('Привет! Я функция')
#
#
# # a = some_func()
# # print(a)
#
# def power(number, pow):
#     print('Функцию вызвали с параметрами', number, pow)
#     power_value = number ** pow
#     return power_value
#
#
# def create_default_user():
#     name = "Василий"
#     age = 27
#     return name, age
#
#
#
# user_name, age = create_default_user()
# print(user_name)
# print(age)

# распаковка параметров (аргументов)
def vector_module(x, y, z):
    return (x ** 2 + y ** 2 + z ** 2) ** .5


# распаковка позиционных параметров
some_list = [2, 3, 4]
res = vector_module(*some_list)
x, y, z = some_list
vector_module(2, 3, 4)
print(res)