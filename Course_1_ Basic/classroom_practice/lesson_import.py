import simple_draw as sd

sd.resolution = (1000, 1000)

x = 10
y = 800

for y in range(100, 301, 100):
    for x in range(50, 1000, 100):
        point = sd.get_point(x, y)
        sd.circle(center_position=point, radius= 40, width= 0, color=sd.random_color())


sd.pause()