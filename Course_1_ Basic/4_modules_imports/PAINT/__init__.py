import random
import simple_draw as sd
import time
sd.resolution = (1400, 750)
def snowfall(xx, xx1, yy, yy1):
    x = []
    y = []
    end_y = []
    for num in range(xx, xx1, 20):
        x.append(num)
    print(x)
    print(len(x))

    for num3 in range(378, 518, 14):
        end_y.append(num3)
    for num4 in range(504, 364, -14):
        end_y.append(num4)
    print("end_y:", end_y)
    print(len(end_y))

    for num1 in range(20):
        num1 = random.randint(yy, yy1)
        y.append(num1)
    print("y:", y)
    print(len(y))

    leng = []
    for num2 in range(20):
        num2 = random.randint(10, 25)
        leng.append(num2)
    print(leng)
    list_cordinate = 0
    print(len(leng))
    while True:

        sd.start_drawing()
        for num_x in range(len(x)):
            point = sd.get_point(x[num_x], y[num_x])
            if y[num_x] > end_y[num_x]:
                sd.snowflake(center=point, length=leng[num_x], color=sd.COLOR_BLACK)
        for num_x in range(len(x)):
            list_cordinate = 0
            if y[num_x] > end_y[num_x]:
                y[num_x] -= random.randint(6, 9)
                x[num_x] += random.randint(5, 10)
                x[num_x] -= random.randint(5, 10)
            else:
                y[num_x] += 300
                end_y[num_x] += 5
            point = sd.get_point(x[num_x], y[num_x])
            sd.snowflake(center=point, length=leng[num_x])
            list_cordinate += 1
        sd.finish_drawing()
        if sd.user_want_exit(0):
            break

        rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                          sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)

        radius = 1050
        sd.start_drawing
        for _ in range(7):
            a = random.randint(0, 6)
            point1 = sd.get_point(500, 0)
            sd.circle(center_position=point1, color=rainbow_colors[a], radius=radius, width=15)
            radius -= 15
        sd.finish_drawing
        sd.sleep(0.1)
        if sd.user_want_exit(0):
            break


def smile(x = 215, y = 185):
    point = sd.get_point(x, y)
    sd.circle(center_position=point, radius=50, width=0)
    r = 13
    point1 = sd.get_point(x-16, y-20)
    v1 = sd.get_vector(start_point=point1, angle=0, length=30, width=3)
    v1.draw(sd.COLOR_BLACK,3)
    v2 = sd.get_vector(start_point=v1.end_point, angle=30, length=15, width=3)
    v2.draw(sd.COLOR_BLACK, 3)
    v3 = sd.get_vector(start_point=v1.start_point, angle=150, length=15, width=3)
    v3.draw(sd.COLOR_BLACK, 3)
    for _ in range(2):
        point6 = sd.get_point(x + 20, y + 20)
        point7 = sd.get_point(x - 20, y + 20)
        sd.circle(center_position=point6, radius=r, color=sd.COLOR_WHITE, width=0)
        sd.circle(center_position=point7, radius=r, color=sd.COLOR_WHITE, width=0)
    for _ in range(2):
        point6 = sd.get_point(x + 20, y + 20)
        point7 = sd.get_point(x - 20, y + 20)
        sd.circle(center_position=point6, radius=5, color=sd.COLOR_BLACK, width=0)
        sd.circle(center_position=point7, radius=5, color=sd.COLOR_BLACK, width=0)
    for _ in range(2):
        point6 = sd.get_point(x + 20, y + 20)
        point7 = sd.get_point(x - 20, y + 20)
        sd.circle(center_position=point6, radius=18, color=sd.COLOR_BLACK, width=2)
        sd.circle(center_position=point7, radius=18, color=sd.COLOR_BLACK, width=2)
    point2 = sd.get_point(x-50, y+40)
    point3 = sd.get_point(x+50, y+55)
    sd.rectangle(point2, point3,color=sd.COLOR_RED, width=0)
    point4 = sd.get_point(x - 30, y + 55)
    point5 = sd.get_point(x + 30, y + 100)
    sd.rectangle(point4, point5, color=sd.COLOR_RED, width=0)
    point1 = sd.get_point(x - 46, y + 28)
    v = sd.get_vector(start_point=point1, angle=330, length=12, width=1)
    v.draw(sd.COLOR_BLACK, 3)
    point1 = sd.get_point(x + 42, y + 28)
    v4 = sd.get_vector(start_point=point1, angle=208, length=8, width=1)
    v4.draw(sd.COLOR_BLACK, 3)
    point1 = sd.get_point(x - 2, y + 22)
    v5 = sd.get_vector(start_point=point1, angle=0, length=5, width=1)
    v5.draw(sd.COLOR_BLACK, 3)
    point1 = sd.get_point(x - 1, y - 1)
    v6 = sd.get_vector(start_point=point1, angle=60, length=10, width=1)
    v6.draw(sd.COLOR_BLACK)
    v7 = sd.get_vector(start_point=v6.end_point, angle=60 + 120, length=10, width=1)
    v7.draw(sd.COLOR_BLACK)
    v8 = sd.get_vector(start_point=v7.end_point, angle=60 + 240, length=10, width=1)
    v8.draw(sd.COLOR_BLACK)

def bricks():
    strx = 0
    stry = 0
    stepx = 75
    stepy = 35
    for k in range(10):
        a = 0
        if k % 2 == 0:
            strx += stepx / 2
            for i in range(6):
                point1 = sd.get_point(strx, stry)
                point2 = sd.get_point(strx + stepx, stry + stepy)
                sd.rectangle(point1, point2, color=sd.COLOR_ORANGE, width=2)
                strx += stepx
                print("hello", k)
        else:
            for i in range(7):
                if a == 6:
                    point1 = sd.get_point(strx, stry)
                    point2 = sd.get_point(strx + stepx / 2, stry + stepy)
                    sd.rectangle(point1, point2, color=sd.COLOR_ORANGE, width=2)
                else:
                    point1 = sd.get_point(strx, stry)
                    point2 = sd.get_point(strx + stepx, stry + stepy)
                    sd.rectangle(point1, point2, color=sd.COLOR_ORANGE, width=2)
                    strx += stepx
                    a += 1
        strx = 0
        stry += stepy


# def rainbow():
#     rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
#                       sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)
#
#     radius = 1050
#     sd.start_drawing
#     for _ in range(7):
#         a = random.randint(0, 6)
#         point1 = sd.get_point(500, 0)
#         sd.circle(center_position=point1, color=rainbow_colors[a], radius=radius, width=15)
#         radius -= 15
#     sd.finish_drawing
#     sd.sleep(1)



def tree():
    point_0 = sd.get_point(900, 5)

    def branch(point, angle, length, delta):
        if length < 10:
            return
        if length < 40:
            c = sd.COLOR_GREEN
        else:
            c = sd.COLOR_DARK_ORANGE
        v1 = sd.get_vector(start_point=point, angle=angle, length=length, width=5)
        v1.draw(c)
        next_point = v1.end_point
        next_angle = angle - (delta + random.randint(-5, 5))
        next_angle_2 = angle + (delta + random.randint(-5, 5))
        next_length = length * (random.randint(6, 9) / 10)
        branch(point=next_point, angle=next_angle, length=next_length, delta=delta)
        branch(point=next_point, angle=next_angle_2, length=next_length, delta=delta)

    # Пригодятся функции
    # sd.random_number()
    branch(point_0, angle=90, length=100, delta=sd.random_number(15, 35))


def roof():
    point = sd.get_point(0, 350)
    v = sd.get_vector(point, 390, 280, 20)
    v.draw(sd.COLOR_RED)
    point1 = v.end_point
    print(point1)
    v1 = sd.get_vector(point1, 690, 283, 20)
    v1.draw(sd.COLOR_RED)