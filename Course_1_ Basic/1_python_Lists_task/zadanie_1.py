# моя семья (минимум 3 элемента, есть еще дедушки и бабушки, если что)
my_family = 'dad, mom, me'
print(my_family)
# список списков приблизителного роста членов вашей семьи
my_family_height = '146, 185, 170'
# Выведите на консоль рост отца в формате
#   Рост отца - ХХ см
print('my dads height: ' + my_family_height[5:8] + 'cm')
# Выведите на консоль общий рост вашей семьи как сумму ростов всех членов
#   Общий рост моей семьи - ХХ см
a = int(my_family_height[0:3]) + int(my_family_height[5:8]) + int(my_family_height[10:13])
print('my family height: ' + str(a))